# Version of the original Dockerfile optimized for Atlassian Bamboo with Postgres preinstalled
# What is inside
# - Build Image (from Dockerfile)
# - PostgreSQL (For use with PostgreSQL unit tests)

FROM php:7.4-apache

LABEL maintainer="kenneth@itachi1706.com"

# Add dependecies
RUN apt-get update && apt-get install -y gnupg git libzip-dev libpng-dev libjpeg-dev libicu-dev libxml2-dev libpq-dev zip locales unixodbc-dev apt-transport-https ca-certificates curl gnupg-agent software-properties-common nano && rm -rf /var/lib/apt/lists*

# Setup MSSQL and PGSQL
# PGSQL PGP Key should be the same key as https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list && echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql17 mssql-tools python3-software-properties software-properties-common postgresql-12 postgresql-client-12 postgresql-contrib-12 && rm -rf /var/lib/apt/lists*
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

# Install PHP Extensions
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install mysqli pdo_mysql pdo_pgsql pgsql zip gd intl xmlrpc soap opcache
RUN pecl install sqlsrv pdo_sqlsrv xdebug
RUN docker-php-ext-enable sqlsrv pdo_sqlsrv xdebug

# Setup Moodle
RUN mkdir -p /opt/moodle && ln -s /opt/moodle /var/www/html/moodle && chown -R www-data:www-data /var/www/html/moodle && mkdir -p /data/moodle/dir && chown -R www-data:www-data /data/moodle/dir && mkdir -p /data/moodle/punit && chown -R www-data:www-data /data/moodle/dir

# Install composer and set PHP memory Limit
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php && php -r "unlink('composer-setup.php');"
RUN echo 'memory_limit = 1G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini;

# Set the locale
RUN sed -i -e 's/# en_AU.UTF-8 UTF-8/en_AU.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG en_AU.UTF-8  
ENV LANGUAGE en_AU:en  
ENV LC_ALL en_AU.UTF-8 
RUN locale-gen en_AU.UTF-8

ADD info.php /var/www/html
WORKDIR /var/www/html/moodle
#RUN service apache2 start

USER postgres

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.
RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" &&\
    createdb -O docker docker &&\
    psql --command "CREATE USER mood WITH SUPERUSER PASSWORD 'P@ssw0rd';" &&\
    createdb -O mood moodle_test
    
# Adjust PostgreSQL configuration so that remote connections to the
# database are possible. And add ``listen_addresses`` to ``/etc/postgresql/9.3/main/postgresql.conf``
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/12/main/pg_hba.conf && echo "listen_addresses='*'" >> /etc/postgresql/12/main/postgresql.conf

USER root

