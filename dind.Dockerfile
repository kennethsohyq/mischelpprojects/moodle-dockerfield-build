# Version of the original Dockerfile optimized for Docker in Docker
# We optimized it to reduce image sizes
# What is inside
# - Build Image (from Dockerfile)
# - Docker (Might be removable actually)

FROM php:7.4-apache

LABEL maintainer="kenneth@itachi1706.com"

# Install initial packages
RUN apt-get update && apt-get install -y gnupg git libzip-dev libpng-dev libjpeg-dev libicu-dev libxml2-dev libpq-dev zip locales unixodbc-dev apt-transport-https ca-certificates curl gnupg-agent software-properties-common nano && rm -rf /var/lib/apt/lists*

# Add MSSQL and docker
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql17 mssql-tools docker-ce docker-ce-cli containerd.io && rm -rf /var/lib/apt/lists*
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

# Add PHP Extensions
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install mysqli pdo_mysql pdo_pgsql pgsql zip gd intl xmlrpc soap opcache
RUN pecl install sqlsrv pdo_sqlsrv xdebug
RUN docker-php-ext-enable sqlsrv pdo_sqlsrv xdebug

# Init Moodle
RUN mkdir -p /opt/moodle && ln -s /opt/moodle /var/www/html/moodle && chown -R www-data:www-data /var/www/html/moodle && mkdir -p /data/moodle/dir && chown -R www-data:www-data /data/moodle/dir && mkdir -p /data/moodle/punit && chown -R www-data:www-data /data/moodle/punit

# Init Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php && php -r "unlink('composer-setup.php');"

# Set limit of PHP
RUN echo 'memory_limit = 1G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini;

# Set the locale
RUN sed -i -e 's/# en_AU.UTF-8 UTF-8/en_AU.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG en_AU.UTF-8  
ENV LANGUAGE en_AU:en  
ENV LC_ALL en_AU.UTF-8 
RUN locale-gen en_AU.UTF-8

ADD info.php /var/www/html
WORKDIR /var/www/html/moodle
#RUN service apache2 start

# For Atlassian 
ENV BAMBOO_USER=bamboo
ENV BAMBOO_GROUP=bamboo
ENV BAMBOO_USER_HOME=/home/${BAMBOO_USER}

RUN addgroup ${BAMBOO_GROUP} && adduser ${BAMBOO_USER} --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --home ${BAMBOO_USER_HOME} --ingroup ${BAMBOO_GROUP} --disabled-password

RUN usermod -aG docker ${BAMBOO_USER}

USER ${BAMBOO_USER}
