# Custom instance of the Atlassian Bamboo image with Docker preinstalled

FROM atlassian/bamboo-server:latest

USER root 

RUN apt-get update && apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common nano && rm -rf /var/lib/apt/lists*

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

RUN apt-get update && apt-get -y install docker-ce docker-ce-cli containerd.io && rm -rf /var/lib/apt/lists*

RUN usermod -aG docker ${BAMBOO_USER}

#USER ${BAMBOO_USER}

ENTRYPOINT ["/entrypoint.sh"]

# HOW TO RUN
# docker volume create --name bambooVolume
# docker run -v bambooVolume:/var/atlassian/application-data/bamboo --name="bamboo" --init -d -p 54663:54663 -p 8686:8085 -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/kennethsohyq/mischelpprojects/moodle-dockerfield-build/bamboo:master
